from django.urls import path

from . import views

urlpatterns = [
    path('', views.TableListView.as_view(), name='index'),
    path('detail/<int:pk>', views.ItemDetailView.as_view(), name='item_detail'),
    path('create_item/', views.create_item, name='create_item'),
    path('edit_item/<int:pk>', views.edit_item, name='edit_item'),
    path('delete_item/<int:pk>', views.delete_item, name='delete_item'),
]
