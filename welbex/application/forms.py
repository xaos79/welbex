from django import forms

from .models import Table


class TableForm(forms.Form):
    """Форма для фильтрации"""

    FIELD = (
        ('name', 'название'),
        ('quantity', 'количество'),
        ('distance', 'расстояние'),
    )
    OPERATION = (
        ('=', 'равно'),
        ('__icontains=', 'содержит'),
        ('__gt=', 'больше'),
        ('__lt=', 'меньше'),
    )

    field_model = forms.ChoiceField(choices=FIELD)
    operation = forms.ChoiceField(choices=OPERATION)
    input_field = forms.CharField(max_length=128)


class CreateItemForm(forms.ModelForm):
    """Форма для создания элемента таблицы"""

    class Meta:
        model = Table
        fields = '__all__'
