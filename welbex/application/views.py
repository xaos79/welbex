from django.views.generic import ListView, DeleteView
from django.shortcuts import render, redirect

from .models import Table
from .forms import TableForm, CreateItemForm


class TableListView(ListView):
    """Класс для отображения таблицы"""

    model = Table
    template_name = 'application/index.html'
    context_object_name = 'table'
    paginate_by = 10

    def get_queryset(self):
        queryset = super().get_queryset()
        method_get = self.request.GET

        if 'field_model' in method_get.keys():

            field_model = self.request.GET['field_model']
            operation = self.request.GET['operation']
            query_search = self.request.GET['input_field']

            if field_model == 'name':
                if operation == '=' or operation == '__icontains=':
                    queryset = eval(f'Table.objects.filter(name{operation}"{query_search}")')
            else:
                queryset = eval(f'Table.objects.filter({field_model}{operation}"{query_search}")')

        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = TableForm()
        return context


class ItemDetailView(DeleteView):
    """Просмотр детальной информации элемента"""

    model = Table
    context_object_name = 'item'
    template_name = 'application/item_detail.html'


def create_item(request):
    """Создание элемента таблицы"""

    if request.method == 'POST':
        form = CreateItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = CreateItemForm()
    return render(request, 'application/create_item.html', {'form': form})


def edit_item(request, pk):
    """Редактирование элемента таблицы"""

    item = Table.objects.get(id=pk)
    if request.method == 'POST':
        form = CreateItemForm(instance=item, data=request.POST)
        if form.is_valid():
            form.save()
    else:
        form = CreateItemForm(instance=item)
    return render(request, 'application/edit_item.html', {'form': form})


def delete_item(request, pk):
    """Удаление элемента таблицы"""

    item = Table.objects.get(id=pk)
    item.delete()
    return redirect('index')
