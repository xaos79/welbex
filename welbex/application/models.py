from django.db import models


class Table(models.Model):
    """Класс таблицы"""

    date = models.DateField(auto_now_add=True, verbose_name='Дата')
    name = models.CharField(max_length=128, verbose_name='Название')
    quantity = models.PositiveIntegerField(verbose_name='Количество')
    distance = models.IntegerField(verbose_name='Расстояние')

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = 'Таблица'
        verbose_name_plural = 'Таблицы'


