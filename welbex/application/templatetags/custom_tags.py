from django import template


register = template.Library()


@register.simple_tag(takes_context=True)
def url_replace(context, **kwargs):
    """Тэг для обновления параметров запроса"""

    data = context['request'].GET.copy()
    for key, value in kwargs.items():
        data[key] = value
    return data.urlencode()
